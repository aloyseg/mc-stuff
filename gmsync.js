/**
	using globalVariableSyncMap the <b>workToDo</b> will be done while having a successfull lock on the element "GM-LOCK_<b>key</b>"
	<hr>
	funtion workToDo(prefixedKey, gMap, gSyncMap){...; return result;}<br>
	has the prefixedKey, the globalMap an its underlying globalSyncMap as arguments
	
	@see http://www.mirthcorp.com/community/fisheye/browse/Mirth/branches/3.6.x/server/src/com/mirth/connect/server/util/GlobalChannelVariableStore.java

	@param {Function} workToDo - your work to be done
	@param {String} key - the 
	@param {Boolean} verbose - default is quiet

	@return {Any} - result of workToDo

	@throws Exception thrown in workToDo 
*/
function globalSyncDoWithKey(workToDo, key, verbose) {
	
	const PREFIX = "GM-LOCK_";
	const prefixedKey = PREFIX+key;
	
	const info = !verbose ? function(){} : function(s) {logger.info("Channel["+channelName+"] "+s)};
	const error = function(s) {logger.error("Channel["+channelName+"] "+s)};
	
	var errorOccured = false;
	var result;
	
	info("trying to get lock on "+prefixedKey+"...");

	// make shure prefixedKey exists
	if ( !globalMap.containsKeySync(prefixedKey) ) {
		globalMap.putSync(prefixedKey, channelName);
		info("created "+prefixedKey+" in globalVariableSyncMap");
	}
	
	// sync'ed section
	try {
		globalMap.lock(prefixedKey);
		info("got lock on "+prefixedKey+" with value="+globalMap.getSync(prefixedKey));
		info(">>> start sync'ed work...");
		result = workToDo(prefixedKey, globalMap, globalMap.globalVariableSyncMap);	
		// we can possibly use workToDo in globalChannelMap as well
		info("<<< sync'ed work done.");
	}
	catch(e) {
		error(e);
		errorOccured = true;
	} 
	finally {
		globalMap.unlock(prefixedKey);
		info("release lock on "+prefixedKey);
	}
	
	if (errorOccured) throw("Channel["+channelName+"] function globalSyncDoWithKey: Exception in funtion workToDo(key, gMap, gSyncMap).")

	return result;
}